# S-N-A-K-E  

A Pascal based game based on the popular Nokia phone game of the same name.  
This is a Year 10 (1999) project by Johnson Zhou and Peter Tran.  

![Screenshot](./img/snake-2-screenshot.jpg)

## Play

Download and install [Turbo Pascal with DosBox](https://turbopascal-wdb.sourceforge.io/).

> `SNAKE2.PAS` seems to be the only working version.